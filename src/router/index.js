import Vue from 'vue'
import VueRouter from 'vue-router'

//Page imports from views
import Dashboard from '@/views/Dashboard'
import Messages from '@/views/Messages'
import Articles from '@/views/Articles'
import Page from '@/views/Page'
import Profile from '@/views/Profile'
import Payments from '@/views/Payments'



Vue.use(VueRouter)

const routes = [{
    path: '/',
    component: Dashboard
  },

  {
    path: '/Messages',
    component: Messages
  },
  {
    path: '/articles',
    component: Articles
  },
  {
    path: '/profile',
    component: Profile
  },
  {
    path: '/orders',
    component: Page
  },
  {
    path: '/payments',
    component: Payments
  },
  // otherwise redirect to home
  {
    path: '*',
    redirect: '/'
  }
]



const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router