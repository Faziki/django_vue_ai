import Vue from 'vue';
import Vuetify from 'vuetify'

Vue.use(Vuetify);

const opts = {
  theme: {
    dark: true,
    options: {
      customProperties: true,
    },
    themes: {
      light: {
        primary: '#a07945',
        secondary: '#424242',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107'
      },
      dark: {
        primary: '#000000',
        secondary: '#FFFFFF',
        accent: '#82B1FF',
        error: '#FF5252',
        info: '#2196F3',
        success: '#4CAF50',
        warning: '#FFC107',
        href: '#FFFFFF'
      }
    },
  },
}

export default new Vuetify(opts)